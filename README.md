## About Project

It's a test project for Adilar Company. It is a simple Event RESTful API:

### Features:

- [x] Authentication, Using JWT and Bearer token.
- [x] CRUD for events
- [x] CRUD for users
- [x] Based on Repository Pattern
- [x] Using Transformers (Thanks to Laravel Fractal) to show highly customisable json outputs
- [x] Set Events for Create and Update Events to Send Notifications, etc...
- [x] Using Policies for ACL configurations
- [x] Using Seeders to populate database

### Todos:
- [ ] Write Unit tests 
- [ ] Implement UI for APIs using Vue or React

### Postman Export:
> To Test Api with Postman you can import `adilar-date-project.postman_collection.json` file
