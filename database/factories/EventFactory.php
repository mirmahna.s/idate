<?php

/** @var Factory $factory */

use App\Entities\Event;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Carbon;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'user_id' => \factory(\App\Entities\User::class),
        'starts' => Carbon::now(),
        'ends' => Carbon::now()->addMinutes($faker->numberBetween(1, 100)),
        'all_day_event' => $faker->boolean(20),
        'description' => $faker->text
    ];
});
