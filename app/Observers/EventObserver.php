<?php

namespace App\Observers;

use App\Entities\Event;
use App\Events\EventCreated;
use App\Events\EventUpdated;

class EventObserver
{
    /**
     * Handle the event "creating" event.
     *
     * @param Event $event
     * @return void
     */
    public function creating(Event $event)
    {
        # sets owner id of event
        $event->user_id = auth()->id();
    }

    /**
     * Handle the event "created" event.
     *
     * @param Event $event
     * @return void
     */
    public function created(Event $event)
    {
        # notify user using email
        event(new EventCreated($event));
    }

    /**
     * Handle the event "updated" event.
     *
     * @param Event $event
     * @return void
     */
    public function updated(Event $event)
    {
        # notify user using email
        event(new EventUpdated($event));
    }

    /**
     * Handle the event "deleted" event.
     *
     * @param Event $event
     * @return void
     */
    public function deleted(Event $event)
    {
        //
    }

    /**
     * Handle the event "restored" event.
     *
     * @param Event $event
     * @return void
     */
    public function restored(Event $event)
    {
        //
    }

    /**
     * Handle the event "force deleted" event.
     *
     * @param Event $event
     * @return void
     */
    public function forceDeleted(Event $event)
    {
        //
    }
}
