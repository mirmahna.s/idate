<?php

namespace App\Http\Controllers;

use App\Entities\Event;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EventCreateRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Repositories\EventRepository;
use App\Validators\EventValidator;

/**
 * Class EventsController.
 *
 * @package namespace App\Http\Controllers;
 */
class EventsController extends Controller
{
    /**
     * @var EventRepository
     */
    protected $repository;

    /**
     * @var EventValidator
     */
    protected $validator;

    /**
     * EventsController constructor.
     *
     * @param EventRepository $repository
     * @param EventValidator $validator
     */
    public function __construct(EventRepository $repository, EventValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Event::class);

        $events = $this->repository->paginate();
        return $this->sendResponse('Events retrieved successfully', $events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EventCreateRequest $request
     *
     * @return Response
     *
     */
    public function store(EventCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $event = $this->repository->create($request->all());

            # Sync Event Attendees
            $event = $this->repository->syncAttendees($event['data']['id'], $request->get('attendees'));

            return $this->sendResponse('Event created.', $event);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessageBag());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Event $event
     * @return Response
     * @throws AuthorizationException
     */
    public function show(Event $event)
    {
        $this->authorize('view', $event);

        $event = $this->repository->find($event->id);
        return $this->sendResponse('Event retrieved successfully', $event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EventUpdateRequest $request
     * @param Event $event
     * @return Response
     * @throws AuthorizationException
     */
    public function update(EventUpdateRequest $request, Event $event)
    {
        $this->authorize('update', $event);

        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $event = $this->repository->update($request->all(), $event->id);

            # Sync Event Attendees
            $event = $this->repository->syncAttendees($event['data']['id'], $request->get('attendees'));

            return $this->sendResponse('Event updated.', $event);
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessageBag());
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return Response
     * @throws AuthorizationException
     */
    public function destroy(Event $event)
    {
        $this->authorize('delete', $event);

        $this->repository->delete($event->id);
        return $this->sendResponse('Event deleted.');
    }
}
