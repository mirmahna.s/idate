<?php

namespace App\Repositories;

use App\Presenters\EventPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EventRepository;
use App\Entities\Event;
use App\Validators\EventValidator;

/**
 * Class EventRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EventRepositoryEloquent extends BaseRepository implements EventRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Event::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return EventValidator::class;
    }

    /**
     * Specify presenter class name
     *
     * @return string
     */
    public function presenter()
    {
        return EventPresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function syncAttendees(int $eventId, string $attendees)
    {
        $attendeesIds = explode(',', $attendees);
        $event = $this->model->findOrFail($eventId);
        $event->attendees()->attach($attendeesIds);
        return $this->parserResult($event);
    }

}
