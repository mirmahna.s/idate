<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class EventValidator.
 *
 * @package namespace App\Validators;
 */
class EventValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title' => 'required|string|max:255',
            'starts' => 'required|date',
            'ends' => 'required|date|after:starts',
            'all_day_event' => 'boolean',
            'description' => 'string'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'title' => 'string|max:255',
            'starts' => 'date',
            'ends' => 'date|after:starts',
            'all_day_event' => 'boolean',
            'description' => 'string'
        ],
    ];
}
