<?php

namespace App\Policies;

use App\Entities\Event;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any events.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        if ($user->is_admin)
            return true;
        return false;
    }

    /**
     * Determine whether the user can view the event.
     *
     * @param User $user
     * @param Event $event
     * @return mixed
     */
    public function view(User $user, Event $event)
    {
        if ($event->owner->id === $user->id || in_array($user->id, $event->attendees->pluck(['id'])->toArray()))
            return true;
        return false;
    }

    /**
     * Determine whether the user can update the event.
     *
     * @param User $user
     * @param Event $event
     * @return mixed
     */
    public function update(User $user, Event $event)
    {
        if ($event->owner->id === $user->id)
            return true;
        return false;
    }

    /**
     * Determine whether the user can delete the event.
     *
     * @param User $user
     * @param Event $event
     * @return mixed
     */
    public function delete(User $user, Event $event)
    {
        if ($event->owner->id === $user->id)
            return true;
        return false;
    }
}
