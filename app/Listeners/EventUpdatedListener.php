<?php

namespace App\Listeners;

use App\Events\EventUpdated;
use App\Notifications\EventUpdatedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class EventUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param EventUpdated $event
     * @return void
     */
    public function handle(EventUpdated $event)
    {
        $event->event->owner->notify(new EventUpdatedNotification($event->event));
    }
}
