<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Event;

/**
 * Class EventTransformer.
 *
 * @package namespace App\Transformers;
 */
class EventTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['users'];

    /**
     * Transform the Event entity.
     *
     * @param Event $model
     *
     * @return array
     */
    public function transform(Event $model)
    {
        return [
            'id' => (int)$model->id,
            'owner' => $model->owner->name,
            'title' => $model->title,
            'starts' => $model->starts,
            'ends' => $model->ends,
            'all_day_event' => (boolean)$model->all_day_event,
            'description' => $model->description
        ];
    }

    public function includeUsers(Event $model)
    {
        return $this->collection($model->attendees, new UserTransformer);
    }
}
