<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Event.
 *
 * @package namespace App\Entities;
 * @property integer id;
 * @property integer user_id;
 * @property string title;
 * @property \DateTime starts;
 * @property \DateTime ends;
 * @property boolean all_day_event;
 * @property string description;
 * @property User owner;
 * @property User[] attendees;
 */
class Event extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'user_id', 'starts', 'ends', 'all_day_event', 'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'all_day_event' => 'boolean',
        'starts' => 'datetime',
        'ends' => 'datetime',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function attendees()
    {
        return $this->belongsToMany(User::class, 'event_user' ,'event_id');
    }

}
