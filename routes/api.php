<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('auth.')
    ->middleware('api')
    ->prefix('auth')
    ->group(function () {
        Route::post('register', 'Auth\RegisterController@register')->name('register');
        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::get('logout', 'Auth\loginController@logout')->name('logout');
        Route::post('refresh', 'Auth\LoginController@refresh')->name('refresh');
        Route::get('user', 'Auth\LoginController@user')->name('user');
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('sendResetLinkEmail');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('reset');
        Route::post('email/resend', 'Auth\VerificationController@resend')->name('resend');
        Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verify');
    });

Route::prefix('v1')->group(function () {
    Route::middleware(['auth:api'])
        ->group(function () {
            Route::apiResource('users', 'UsersController', ['except' => ['store']]);
            Route::apiResource('events', 'EventsController');
        });
});
